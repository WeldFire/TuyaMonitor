FROM python:3

WORKDIR /

RUN apt-get update && apt-get install -y python-crypto

COPY requirements.txt /requirements.txt

RUN pip install -r requirements.txt

COPY monitor.py /monitor.py

CMD ["python", "./monitor.py"]