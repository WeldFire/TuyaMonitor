Tuya Monitor
============

This is a python application that will allow you to monitor power data reported from Tuya smart switches and broadcast the findings to a mqtt broker of your choice. This can be useful when you wish to view the power data in a different application such as HomeAssitant.

---

## Repo Location
https://gitlab.com/WeldFire/TuyaMonitor

## Local Setup
Clone the repo to your desktop and run `pip install -r requirements.txt` to install all of the dependencies.

Create a `config.json` file from `config-example.json` to specify your devices and mqtt server connection details.

To get your device keys checkout step 2 here [Tuya Setup](https://github.com/clach04/python-tuya/wiki). You may need to downgrade your application if you choose to go the Android route to the lowest version that you can find. SmartLife version 3.4.1 is known to be a working version.

Run the application as `python monitor.py`

---
## Variables
### Config.json
| Name                | Default            | Required                 | Type                                                                     |
|---------------------|--------------------|--------------------------|--------------------------------------------------------------------------|
| plug.id             | N/A                | true                     | Device ID found in your mobile app                                       |
| plug.ip             | N/A                | true                     | Device IP found on your network                                          |
| plug.key            |                    | If version 3.3 or higher | KEY (Obtain from https://github.com/clach04/python-tuya/wiki/Setup)      |
| plug.version        | 3.3                | false                    | Version number (firmware 1.0.5+ == 3.3 else 3.1)                         |
| plug.retry.attempts | 10                 | false                    | Integer defining how many times to try a plug before giving up           |
| plug.retry.delay    | 1                  | false                    | Float defining how long to wait between plug query retries               |
| mqtt.ip             | 192.168.1.123      | false                    | The IP address of your mqtt broker                                       |
| mqtt.username       | homeassistant      | false                    | The username to access your mqtt broker                                  |
| mqtt.password       | homeassistant      | false                    | The password to access your mqtt broker                                  |
| mqtt.topic          | devices/tuya/plug/ | false                    | The path of the topic to write to in your mqtt broker                    |
| mqtt.port           | 1883               | false                    | The port of your mqtt broker                                             |
| mqtt.retain         | false              | false                    | boolean declaring if the messages should be retained on your mqtt broker |
| update-frequency    | 10                 | false                    | Seconds between group refreshes                                          |


### Envronment
| Name          | Default     | Required | Type                                      |
|---------------|-------------|----------|-------------------------------------------|
| logging.level | INFO        | false    | String Logging Level (INFO, DEBUG, ERROR) |
| config_path   | config.json | false    | Path to your config.json                  |


---
## Docker
### Simple
```
docker run -d -v /local/config.json:/config.json weldfire/tuya-monitor
```

#### Docker Compose
```
services:
  tuya-monitor:
    image: weldfire/tuya-monitor
    restart: unless-stopped
    container_name: "tuya-monitor"
    environment:
      - TZ=America/Chicago
    volumes:
      - /local/config.json:/config.json
```

---
## Contributors
Author: Keagan Williams
