#!/usr/bin/python

import datetime
import json
import logging
import os
import traceback

from threading import Timer
from time import sleep

import paho.mqtt.client as mqtt
import tinytuya

log = logging.getLogger("monitor")
console = logging.StreamHandler()

LOG_FORMAT = '%(asctime)s\t%(levelname)s -- %(processName)s %(filename)s:%(lineno)s -- %(message)s'
console.setFormatter(logging.Formatter(LOG_FORMAT))

log.addHandler(console)
log.setLevel(os.getenv('logging.level', "INFO"))

def get_device_power(device_status, watt_index, current_index, voltage_index, switch_index=1):
    returned = {}
    device_dps = device_status['dps']
    returned["switch"] = device_dps[str(switch_index)] # Switch State
    try:
        returned["power"] = (float(device_dps[str(watt_index)])/10.0) # Power - W
        returned["current"] = float(device_dps[str(current_index)]) # Current - mA
        returned["voltage"] = (float(device_dps[str(voltage_index)])/10.0) # Voltage - V
    except Exception as ex:
        returned["result"] = "Unable to parse full device status"
        log.error("Failed to parse device status %s, %s", str(device_status), str(ex))

    return returned

def get_current_status(plug):
    retry_config = plug.get("retry", {})
    retry_attempts = retry_config.get('attempts', 10)
    retry_delay = retry_config.get('delay', 1)

    plug_version = plug.get('version', '3.3')
    plug_name = plug.get('name', None)
    plug_key = plug.get('key')
    plug_id = plug.get('id')
    plug_ip = plug.get('ip')

    message_bad_response = f"Unknown response from plug {get_plug_display_name(plug)} ({plug_ip})."

    assert None not in [plug_id, plug_ip, plug_key], "Plug ID, IP, and Key must be set!"

    returned = {}
    attempts = 0
    now = datetime.datetime.utcnow()
    iso_time = now.strftime("%Y-%m-%dT%H:%M:%SZ")
    returned["datetime"] = iso_time
    returned["deviceid"] = plug_id
    if plug_name:
        returned["name"] = plug_name

    while attempts < retry_attempts:
        try:
            device = tinytuya.OutletDevice(plug_id, plug_ip, plug_key)
            if device:
                if plug_version == '3.3':
                    device.set_version(3.3)

                device_status = device.status()

                dps_keys = device_status['dps'].keys()

                if plug_version == '3.3' and ('19' in dps_keys):
                    returned.update(get_device_power(device_status, 19, 18, 20))
                else:
                    if '5' in dps_keys:
                        returned.update(get_device_power(device_status, 5, 4, 6))
            else:
                returned['result'] = message_bad_response
            break
        except KeyboardInterrupt:
            break
        except Exception as ex:
            attempts+=1
            if attempts < retry_attempts:
                sleep(retry_delay)
            else:
                last_exception = ex

    if attempts >= retry_attempts:
        returned["result"] = message_bad_response
        log.error("Failed to update plug %s, last error '%s'", get_plug_display_name(plug), str(last_exception))
        traceback.print_tb(last_exception.__traceback__)

    return json.dumps(returned)


def send_mqtt_update(config, deviceid, payload):
    username = config.get('username', 'homeassistant')
    password = config.get('password', 'homeassistant')
    topic = config.get('topic', 'devices/tuya/plug/')
    ip_address = config.get('ip', '192.168.1.123')
    retain = config.get('retain', False)
    port = config.get('port', 1883)

    mqtt_client = mqtt.Client(username)
    mqtt_client.username_pw_set(username, password)
    mqtt_client.connect(ip_address, port)
    mqtt_client.publish(f"{topic}{deviceid}", payload, retain=retain)
    mqtt_client.loop(2)
    mqtt_client.loop_stop()


def get_plug_display_name(plug):
    plug_ip = plug.get("ip", "unknown")
    plug_id = plug.get("id", "unknown")
    plug_name = plug.get("name", None)

    if not plug_name:
        plug_name = f"{plug_ip} ({plug_id})"

    return plug_name

def update_plugs(plug_config):
    plugs = plug_config.get("plugs",[])
    log.info("Attempting to refresh the status for %d plugs", len(plugs))

    for plug in plugs:
        display_name = get_plug_display_name(plug)
        try:
            log.debug("Attempting to get stats for %s", display_name)
            current_status = get_current_status(plug)
            log.info(current_status)

            send_mqtt_update(plug_config.get("mqtt"), plug.get("id"), current_status)
            log.debug("Finished monitoring plug %s", display_name)
        except Exception as ex:
            log.error("Unable to monitor plug %s, %s", display_name, str(ex))

    log.info("Finished monitoring all plugs")

def update_plugs_consistantly():
    config = load_config() #Super short update frequencies could be painful here
    update_frequency = config.get('update-frequency', 10)

    update_plugs(config)

    log.info("Waiting for %f seconds before checking again\n", update_frequency)
    update_timer = Timer(update_frequency, update_plugs_consistantly)
    update_timer.start()

GLOBAL_CONFIG = None
def load_config():
    config_path = os.getenv('config_path', 'config.json')
    assert os.path.exists(config_path), f"Unable to find the configuration file at {config_path}"

    config = {}
    try:
        with open(config_path, 'r') as config_file:
            config = json.load(config_file)

        global GLOBAL_CONFIG
        if GLOBAL_CONFIG != config:
            log.info("New configuration data has been loaded")
            log.debug("Configuration Data:\n%s", json.dumps(config, sort_keys=True, indent=4))
            GLOBAL_CONFIG = config
    except Exception as ex:
        log.error("Unable to parse the configuration file as json, bad format?, %s", str(ex))

    return config

if __name__ == "__main__":
    update_plugs_consistantly()
